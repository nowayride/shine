package config

import (
	"fmt"

	"github.com/Jeffail/gabs"
	"github.com/levigross/grequests"

	"encoding/json"
	"io/ioutil"
)

// Config store a configuration struct
type Config struct {
	Username string
	Hub      string
}

// Config.URL return the full API endpoint
func (c Config) URL(path string) string {
	return fmt.Sprintf("%s/%s/%s", c.Hub, c.Username, path)
}

// GetHubIP from hue nupnp
func GetHubIP() (h string, err error) {
	resp, err := grequests.Get("https://www.meethue.com/api/nupnp", nil)
	if err != nil {
		return
	}

	json, err := gabs.ParseJSON(resp.Bytes())
	if err != nil || resp.Ok != true {
		err = fmt.Errorf(fmt.Sprintf("Invalid response from meethue.com: %s", err))
		return
	}

	children, _ := json.Children()
	for _, child := range children {
		v, ok := child.Path("internalipaddress").Data().(string)
		if ok {
			h = fmt.Sprintf("http://%s/api", v)
			break
		}
	}
	return
}

// GetUsername from the hue hub
func GetUsername(hub string) (u string, err error) {
	resp, err := grequests.Post(hub, &grequests.RequestOptions{
		JSON:   map[string]string{"devicetype": "shine#golang"},
		IsAjax: true,
	})
	if err != nil {
		return
	}

	json, err := gabs.ParseJSON(resp.Bytes())
	if err != nil {
		return
	}

	children, _ := json.Children()
	for _, child := range children {
		if child.Exists("error") {
			err = fmt.Errorf("%v", child.Path("error.description"))
			return
		}
		u = child.Path("success.username").Data().(string)
	}
	return
}

// Generate a configuration file
func Generate(file string, c *Config) (err error) {
	h, err := GetHubIP()
	if err != nil {
		return
	}

	c.Hub = h

	u, err := GetUsername(c.Hub)
	if err != nil {
		return
	}

	c.Username = u
	err = nil
	f, err := json.Marshal(c)
	ioutil.WriteFile(file, f, 0644)
	return
}

// Read loads a configuration file
func Read(file string) (c Config, err error) {
	f, err := ioutil.ReadFile(file)
	if err != nil {
		err = Generate(file, &c)
	} else {
		err = json.Unmarshal(f, &c)
	}
	return
}
