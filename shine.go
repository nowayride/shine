package main

import (
	"log"

	"bitbucket.org/nowayride/shine/config"
)

func main() {
	f := "./config.json"
	c, err := config.Read(f)
	if err != nil {
		log.Fatal(err)
	}
	log.Print("using config ", c)
}
